import 'package:flutter/material.dart';
import 'package:homework1/helpers/screennav.dart';
import 'package:homework1/helpers/textstyle.dart';
import 'package:homework1/models/Product.dart';
import 'package:homework1/screens/third.dart';


List<Product> productsList = [
  Product("Cereale", 12),
  Product("Salata", 9),
  Product("Lapte", 4)
];



class Shop extends StatefulWidget {
  @override
  _ShopState createState() => _ShopState();
}

class _ShopState extends State<Shop> {

  String string;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         backgroundColor: Colors.black,
         title: CustomText(text: "Shop", color: Colors.white,),
       ),
      body:Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: CustomText(text: "Shop",size: 30,color: Colors.black,),
          ),
          SizedBox(height: 10,),
          
          Container(
            height: 230,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: productsList.length,
                itemBuilder: (_,index){
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Stack(
                      children: [
                        Container(
                          width: 200,
                          height: 200,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[100],
                                blurRadius: 7,
                                offset: Offset(3,3)
                              )
                            ]
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(60,20,8,8),
                              child: CustomText(text: productsList[index].name,size: 22, color: Colors.black,),
                            ),
                            SizedBox(height:20),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(60,20,8,8),
                              child: CustomText(text: "${productsList[index].price}"),
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                },
            ),
          ),
          TextField(
            onSubmitted: (pattern){
              string = pattern;
            },
            textAlign: TextAlign.center,


          ),
          FloatingActionButton(onPressed: (){
            changeScreen(context, Third(string: string));
          })
        ],
      ),
    );
  }
}
