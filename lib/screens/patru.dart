import 'package:flutter/material.dart';
import 'package:homework1/helpers/screennav.dart';
import 'package:homework1/helpers/textstyle.dart';
import 'package:homework1/screens/third.dart';

List<String> strings = [
  "Stefan", "Mihnea" , "Andrica" , "Blaga" , "Lia"
];

class Patru extends StatefulWidget {

  @override
  _PatruState createState() => _PatruState();
}

class _PatruState extends State<Patru> {

  String string1;
  String string2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: CustomText(text: "Page Four", size: 22, color: Colors.white,),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: 200,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 7,
                    offset: Offset(2,2)
                  )
                ]
              ),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "  Text 1",
                  border: InputBorder.none,
                ),
                onSubmitted: (pattern){
                  string1 = pattern;
                },

              ),
            ),
          ),
          FloatingActionButton(onPressed: (){
            strings.add(string1);
            changeScreen(context, Third(string: string1,));
          },backgroundColor: Colors.black,),

          SizedBox(height: 10,),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: 200,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 7,
                        offset: Offset(2,2)
                    )
                  ]
              ),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "   Text 2",
                  border: InputBorder.none,
                ),
                onSubmitted: (pattern){
                  string2 = pattern;
                },

              ),
            ),
          ),
          FloatingActionButton(onPressed: (){
            strings.add(string2);
            changeScreen(context, Third(string: string2,));
          },backgroundColor: Colors.black,),
        ],
      ),

    );

  }
}


