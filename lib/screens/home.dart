import 'package:flutter/material.dart';
import 'package:homework1/helpers/screennav.dart';
import 'package:homework1/helpers/textstyle.dart';
import 'package:homework1/models/user.dart';
import 'dart:core';

import 'package:homework1/screens/shop.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  User user1 = new User("colt@gmail.com", "parolacolt") ;
  User user2 = new User("mihaila@gmail.com", "parolamihaila") ;
  User user3 = new User("nuta@gmail.com", "parolanuta") ;
  User user4 = new User("sacu@gmail.com", "parolasacu") ;
  User user5 = new User("blaga@gmail.com", "parolablaga") ;







  String user;
  String password;

  final myController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: CustomText(text: "Login Page",color: Colors.white,),
      ),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: TextField(
                    onSubmitted: (pattern){
                          user = pattern;
                          print(user);
                    },
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Email",
                        icon: Icon(Icons.email, color: Colors.black,)
                    ),
                  ),
                ),

              ),
            ),
      Padding(
        padding: const EdgeInsets.all(12),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: TextField(
              onSubmitted: (pattern){
                password = pattern;
                print(password);
              },
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Password",
                  icon: Icon(Icons.lock, color: Colors.black,)
              ),
            ),
          ),
          ),
      ),
            FloatingActionButton(onPressed: (){
              print(this.user +" "+ this.password);
              if(user1.email == user && user1.password == password){
                changeScreen(context, Shop());
              }
              if(user2.VerifyUser(user, password)){
                changeScreen(context, Shop());
              }
              if(user3.VerifyUser(user, password)){
                changeScreen(context, Shop());
              }
              if(user4.VerifyUser(user, password)){
                changeScreen(context, Shop());
              }
              if(user5.VerifyUser(user, password)){
                changeScreen(context, Shop());
              }
            })


          ],
        ),
      ),
    );

  }
}
