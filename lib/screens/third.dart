import 'package:flutter/material.dart';
import 'package:homework1/helpers/screennav.dart';
import 'package:homework1/helpers/textstyle.dart';
import 'package:homework1/screens/patru.dart';



class Third extends StatefulWidget {

  String string;

  Third({this.string});

  @override
  _ThirdState createState() => _ThirdState();
}

class _ThirdState extends State<Third> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         backgroundColor: Colors.black,
        title: CustomText(text: "Third", color: Colors.white,),
      ),
      body: Column(
       children: [
         AlertDialog(
           title: Text(widget.string),
           content: SingleChildScrollView(
             child: ListBody(
               children: [
                 Text('This is a demo'),
                 Text('DEMOOO')
               ],
             ),
           ),
           actions: [
             TextButton(onPressed: (){changeScreen(context, Patru());}, child: Text('Approve')),
           ],

         )
       ],
      ),
    );

  }
}
